#ifndef POLYGON_H
#define POLYGON_H

#include "qmapcontroladdon_global.h"
#include "surface.h"
#include "point.h"

namespace qmapcontrol
{
	//! Draws a filled polygon into the map
	/*! This is a convenience class for Surface.
	 * Polygon has minimum 3 points - to draw a triangle.
	 *
	 * From the OGC Candidate Implementation Specification:
	 * "A Polygon is a planar Surface defined by 1 exterior boundary and 0 or more interior boundaries.
	 * Each interior boundary defines a hole in the Polygon."
	 *
	 * @author Alexey Melnikov <lexam91@gmail.com>
	 */
	class QMAPCONTROLADDON_EXPORT Polygon : public Surface
	{
	public:
		//! constructor
		/*!
		 * The constructor of a Polygon takes a list of Points to form a surface
		 * @param points a list of points
		 * @param name the name of the Polygon
		 * @param filled used to determine drawn surface to be painted over with color from the given pen
		 * @param pen a QPen can be used to modify the look of the surface.
		 * @see http://doc.trolltech.com/4.3/qpen.html
		 */
		Polygon( QList <Point*> const points, QString name = QString(), bool filled = false, QPen *pen = 0 );
		virtual ~Polygon();

		//! returns the points of the Polygon
		/*!
		 * @return  a list with the points of the Polygon
		 */
		QList <Point *> points();

		//! Adds a point at the end of the Polygon.
		/*!
		 * @param point the point which should be added to the Polygon
		 */
		void addPoint( Point *point );

		//! Sets the given list as points of the Polygon
		//! NOTE: these points will get reparented and cleaned up automatically
		/*!
		 * @param points the points which should be set for the Polygon
		 */
		void setPoints( QList <Point *> const points );

		//! Returns the number of Points the Polygon consists of
		/*!
		 * @return the number of the Polygon´s Points
		 */
		int numberOfPoints() const;

		//! Returns true if the Polygon has Childs
		/*!
		 * This is equal to: numberOfPoints() > 0
		 * @return true it the Polygon has Childs (=Points)
		 * @see clickedPoints()
		 */
		virtual bool hasPoints() const;

		//! Returns the bounding box (rect) that contains all points of the Polygon
		/*!
		 * @return the rect that contains all Polygon's points
		 */
		virtual QRectF boundingBox();

		//! Returns true if the given Point located inside Polygon.
		/*!
		 * @param point the point which should be checked for location inside Polygon
		 */
		bool touches( const Point &point );

		//! Determine drawn surface to be painted over with color from the given in constructor pen
		/*!
		 * @param filled used to determine surface for painting over
		 */
		inline void setFilled( bool filled ) { _isFilled = filled; }

		//! Returns determination of drawn surface painted over with color from the given in constructor pen
		/*!
		 * @return true if surface painted over, otherwise returns false
		 */
		inline QColor isFilled() const { return _isFilled; }

	protected:
		virtual bool Touches( Point *geom, const MapAdapter *mapadapter );
		virtual bool Touches( Geometry *geom, const MapAdapter *mapadapter );
		virtual void draw( QPainter *painter, const MapAdapter *mapadapter, const QRect &screensize, const QPoint offset );

	private:
		void removePoints();

		QList <Point *> _childPoints;
		bool _isFilled;
	};
}

#endif // POLYGON_H
