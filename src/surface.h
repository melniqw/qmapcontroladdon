#ifndef SURFACE_H
#define SURFACE_H

#include "qmapcontroladdon_global.h"
#include "geometry.h"

namespace qmapcontrol
{
	//! A Surface Geometry, implemented to fullfil OGC Spec
	/*!
	 * The Surface class is used by Polygon and Circle (variation of Polygon with
	 * many points beyond and around the center at the same distance)
	 * as parent class.
	 * This is an abstract class and could not be user directly.
	 *
	 * From the OGC Candidate Implementation Specification:
	 * "A Surface is a 2-dimensional geometric object.
	 * A simple Surface consists of a single “patch” that is associated with
	 * one “exterior boundary” and 0 or more “interior” boundaries. Simple
	 * Surfaces in 3-dimensional space are isomorphic to planar Surfaces.
	 * Polyhedral Surfaces are formed by “stitching” together simple Surfaces
	 * along their boundaries, polyhedral Surfaces in 3-dimensional space may
	 * not be planar as a whole."
	 *
	 * @author Melnikov Alexey <lexam91@gmail.com>
	 */
	class QMAPCONTROLADDON_EXPORT Surface : public Geometry
	{
		Q_OBJECT
	public:
		virtual ~Surface();

	protected:
		Surface(QString name = QString());
		virtual void draw(QPainter* painter, const MapAdapter* mapadapter, const QRect &screensize, const QPoint offset) = 0;
	};
}

#endif // SURFACE_H
