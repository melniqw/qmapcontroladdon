#include "polygon.h"

namespace qmapcontrol
{
	Polygon::Polygon( QList <Point *> const points, QString name, bool filled, QPen *pen )
		: Surface( name )
	{
		GeometryType = "Polygon";
		setPoints( points );
		_isFilled = filled;
		mypen = pen;

	}

	Polygon::~Polygon()
	{
		removePoints();
	}

	QList<Point *> Polygon::points()
	{
		return _childPoints;
	}

	void Polygon::addPoint( Point *point )
	{
		_childPoints.append( point );
	}

	void Polygon::setPoints( QList <Point *> const points )
	{
		removePoints();
		_childPoints = points;
	}

	int Polygon::numberOfPoints() const
	{
		return _childPoints.count();
	}

	void Polygon::removePoints()
	{
		qDeleteAll( _childPoints );
		_childPoints.clear();
	}

	bool Polygon::hasPoints() const
	{
		return _childPoints.size() > 0 ? true : false;
	}

	QRectF Polygon::boundingBox()
	{
		qreal minlon=180;
		qreal maxlon=-180;
		qreal minlat=90;
		qreal maxlat=-90;
		foreach( Point *point, _childPoints )
		{
			if( point->longitude() < minlon ) minlon = point->longitude();
			if( point->longitude() > maxlon ) maxlon = point->longitude();
			if( point->latitude() < minlat ) minlat = point->latitude();
			if( point->latitude() > maxlat ) maxlat = point->latitude();
		}
		QPointF min = QPointF( minlon, minlat );
		QPointF max = QPointF( maxlon, maxlat );
		QPointF dist = max - min;
		QSizeF si = QSizeF( dist.x(), dist.y() );
		return QRectF( min, si );
	}

	bool Polygon::touches( const Point &point )
	{
		int count = 0;
		for( int i=0; i < _childPoints.size(); i++ )
		{
			int j = ( i+1 ) % _childPoints.size();
			if( ( _childPoints.at( i )->longitude() < point.longitude() ) && ( _childPoints.at( j )->longitude() < point.longitude() ) )
				continue;
			if( _childPoints.at( i )->latitude() == _childPoints.at( j )->latitude() )
				continue;
			if( ( _childPoints.at( i )->latitude() > point.latitude() ) && ( _childPoints.at( j )->latitude() > point.latitude() ) )
				continue;
			if( ( _childPoints.at( i )->latitude() < point.latitude() ) && ( _childPoints.at( j )->latitude() < point.latitude() ) )
				continue;
			if( std::max( _childPoints.at( i )->latitude(), _childPoints.at( j )->latitude() ) == point.latitude() )
			{
				count++;
			}
			else
			{
				if( std::min( _childPoints.at( i )->latitude(), _childPoints.at( j )->latitude() ) == point.latitude() )
				{
					continue;
				}
				else
				{
					qreal t = ( point.latitude() - _childPoints.at( i )->latitude() ) / ( _childPoints.at( j )->latitude() - _childPoints.at( i )->latitude() );
					if( _childPoints.at( i )->longitude() + t*( _childPoints.at( j )->longitude() - _childPoints.at( i )->longitude() ) >= point.longitude() )
						count++;
				}
			}
		}
		return count & 1;
	}

	bool Polygon::Touches( Point *geom, const MapAdapter *mapadapter )
	{
		if( touches( *geom ) )
		{
			emit geometryClicked( this, QPoint( 0, 0 ) );
			return true;
		}
		return false;
	}

	bool Polygon::Touches( Geometry *geom, const MapAdapter *mapadapter )
	{
		return false;
	}

	void Polygon::draw( QPainter *painter, const MapAdapter *mapadapter, const QRect &screensize, const QPoint offset )
	{
		if( !visible )
			return;

		QPolygon polygon = QPolygon();
		foreach( Point *point, _childPoints )
			polygon << mapadapter->coordinateToDisplay( point->coordinate() );

		if( mypen != 0 )
		{
			painter->save();
			painter->setPen( *mypen );
		}
		painter->drawPolygon( polygon, Qt::OddEvenFill );
		if( _isFilled )
		{
			QPainterPath fillPath;
			fillPath.addPolygon( polygon );
			painter->fillPath( fillPath, QBrush( painter->pen().color() ) );
		}
		if( mypen != 0 )
		{
			painter->restore();
		}
	}
}

