#include "circle.h"

namespace qmapcontrol
{
	Circle::Circle( qreal x, qreal y, quint32 radius, QString name, bool filled, QPen *pen)
		: Surface( name )
	{
		GeometryType = "Circle";
		_x = x;
		_y = y;
		_radius = radius;
		_isFilled = filled;
		mypen = pen;
	}

	Circle::~Circle()
	{
	}

	QList <Point *> Circle::points()
	{
		QList <Point *> points;
		return points;
	}

	bool Circle::hasPoints() const
	{
		return false;
	}

	QRectF Circle::boundingBox()
	{
		return QRectF();
	}

	bool Circle::touches( const Point &point )
	{
//		qreal x = pow( sin((lat2-lat1)*M_PI/360),2) + cos(lat1*M_PI/180) * cos(lat2*M_PI/180) * pow(sin((lon2-lon1)*M_PI/360),2);
//		qreal distance = 6367444.657 * ( 2 * asin( sqrt(x) ) );
		qreal x = pow( sin((point.latitude()-_y)*M_PI/360),2) + cos(_y*M_PI/180) * cos(point.latitude()*M_PI/180) * pow(sin((point.longitude()-_x)*M_PI/360),2);
		qreal distance = 6367444.657 * ( 2 * asin( sqrt(x) ) );
		if( distance <= _radius )
			return true;
		return false;
	}

	bool Circle::Touches( Point *geom, const MapAdapter *mapadapter )
	{
		if( touches( *geom ) )
		{
			emit geometryClicked( this, QPoint( 0, 0 ) );
			return true;
		}
		return false;
	}

	bool Circle::Touches( Geometry *geom, const MapAdapter *mapadapter )
	{
		return false;
	}

	void Circle::draw( QPainter *painter, const MapAdapter *mapadapter, const QRect &screensize, const QPoint offset )
	{
		if( !visible )
			return;

		QList <qreal> distanceList;
		distanceList << 5000000 << 2500000 << 1250000 << 625000 << 312500 << 156250 << 78125 << 39062.5 << 19531.25 << 9765.625 << 4882.8125 << 2441.40625 << 1220.703125 << 610.3515625 << 305.17578125 << 152.58689 << 76.2939453 << 38.14697265 << 19.073486328;
		int zoom = mapadapter->currentZoom();
		qreal displayRadius = _radius * 100 / ( distanceList.at( zoom ) / 0.597164 );

		QPointF center = mapadapter->coordinateToDisplay(QPointF( _x, _y ));
		if( mypen != 0 )
		{
			painter->save();
			painter->setPen( *mypen );
		}
		painter->drawEllipse( center, displayRadius, displayRadius );
		if( _isFilled )
		{
			QPainterPath fillPath;
			fillPath.addEllipse( center, displayRadius, displayRadius );
			painter->fillPath( fillPath, QBrush( painter->pen().color() ) );

		}
		if( mypen != 0 )
		{
			painter->restore();
		}

//		qreal degrees = ( (qreal)_radius * 2 / 1000 ) / 111.111;
//		QPointF topLeft = mapadapter->coordinateToDisplay( QPointF( _x - degrees, _y + degrees ) );
//		QPointF bottomRight = mapadapter->coordinateToDisplay( QPointF( _x + degrees, _y - degrees ) );
//		QRectF rect = QRectF( topLeft, bottomRight );

////		QPointF center = mapadapter->coordinateToDisplay(QPointF( _x, _y ));
//		if( mypen != 0 )
//		{
//			painter->save();
//			painter->setPen( *mypen );
//		}
//		painter->drawEllipse( rect );
//		if( _isFilled )
//		{
//			QPainterPath fillPath;
//			fillPath.addEllipse( rect );
//			painter->fillPath( fillPath, QBrush( painter->pen().color() ) );

//		}
//		if( mypen != 0 )
//		{
//			painter->restore();
//		}
	}
}

