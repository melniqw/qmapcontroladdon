#ifndef CIRCLE_H
#define CIRCLE_H

#include "qmapcontroladdon_global.h"
#include "surface.h"
#include "point.h"

#include <QPen>

namespace qmapcontrol
{
	//! Draws a filled circle into the map
	/*! This is a convenience class for Surface.
	 * Class circle implemented as a variation of Polygon with
	 * many points beyond and around the center at the same distance.
	 *
	 * From the OGC Candidate Implementation Specification:
	 * "A Polygon is a planar Surface defined by 1 exterior boundary and 0 or more interior boundaries.
	 * Each interior boundary defines a hole in the Polygon."
	 *
	 * @author Alexey Melnikov <lexam91@gmail.com>
	 */
	class QMAPCONTROLADDON_EXPORT Circle : public Surface
	{
	public:
		//! constructor
		/*!
		 * The constructor creates a Circle takes position and radius to form a surface.
		 * @param x longitude
		 * @param y latitude
		 * @param radius radius of the circle in meters
		 * @param name the name of the Circle
		 * @param filled used to determine drawn surface to be painted over with color from the given pen
		 * @param pen a QPen can be used to modify the look of the surface.
		 * @see http://doc.trolltech.com/4.3/qpen.html
		 */
		Circle( qreal x, qreal y, quint32 radius = 10, QString name = QString(), bool filled = false, QPen *pen = 0 );
		virtual ~Circle();

		//! Used to inherit Geometry's pure virtual methods, always returns an empy list of Points
		/*!
		 * @return an empty list of Points
		 */
		QList <Point *> points();

		//! Used to inherit Geometry's pure virtual methods, always returns false
		/*!
		 * @return false
		 */
		virtual bool hasPoints() const;

		//! Used to inherit Geometry's pure virtual methods, always returns empty QRectF()
		/*!
		 * @return empty QRectF()
		 */
		virtual QRectF boundingBox();

		//! Returns true if the given Point located inside Circle.
		/*!
		 * @param point the point which should be checked for location inside Circle
		 */
		bool touches( const Point &point );

		//! returns the longitude of the point
		/*!
		 * @return the longitude of the point
		 */
		inline qreal longitude() const { return _x; }

		//! returns the latitude of the point
		/*!
		 * @return the latitude of the point
		 */
		inline qreal latitude() const { return _y; }

		//! Sets Circle radius in metres
		/*!
		 * @param radius radius in metres
		 */
		inline void setRadius( quint32 radius ) { _radius = radius; }

		//! Returns Circle's current radius in metres
		/*!
		 * @return current radius in metres
		 */
		inline quint32 radius() const { return _radius; }

		//! Determine drawn surface to be painted over with color from the given in constructor pen
		/*!
		 * @param filled used to determine surface for painting over
		 */
		inline void setFilled( bool filled ) { _isFilled = filled; }

		//! Returns determination of drawn surface painted over with color from the given in constructor pen
		/*!
		 * @return true if surface painted over, otherwise returns false
		 */
		inline QColor isFilled() const { return _isFilled; }

	protected:
		virtual bool Touches( Point *geom, const MapAdapter *mapadapter );
		virtual bool Touches( Geometry *geom, const MapAdapter *mapadapter );
		virtual void draw( QPainter *painter, const MapAdapter *mapadapter, const QRect &screensize, const QPoint offset );

	private:
		qreal _x;
		qreal _y;
		quint32 _radius;
		bool _isFilled;
	};
}

#endif // CIRCLE_H
