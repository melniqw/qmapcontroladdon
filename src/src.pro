include( ../3rdparty/QMapControl/QMapControl/QMapControl.pri )
INCLUDEPATH += ../3rdparty/QMapControl/QMapControl/src/

TARGET = qmapcontroladdon
TEMPLATE = lib
QT += network
QT += gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
greaterThan(QT_MAJOR_VERSION, 4): cache()

VERSION = 0.1.0.0

DEFINES += QMAPCONTROLADDON_LIBRARY

MOC_DIR = tmp
OBJECTS_DIR = obj
DESTDIR = ../Samples/bin

HEADERS += qmapcontroladdon_global.h
SOURCES += surface.cpp
HEADERS += surface.h
SOURCES += polygon.cpp
HEADERS += polygon.h
SOURCES += circle.cpp
HEADERS += circle.h
