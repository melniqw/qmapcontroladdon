include( ../../QMapControlAddon.pri )
INCLUDEPATH += ../../src/
include( ../../3rdparty/QMapControl/QMapControl/QMapControl.pri )
INCLUDEPATH += ../../3rdparty/QMapControl/QMapControl/src/

QT += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Test
TEMPLATE = app

DESTDIR = bin
MOC_DIR = moc
OBJECTS_DIR = obj
UI_DIR  = ui

VPATH += src/

SOURCES += main.cpp
SOURCES += mainwindow.cpp
HEADERS += mainwindow.h
FORMS   += mainwindow.ui
