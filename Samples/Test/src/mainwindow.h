#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>

#include "mapadapter.h"
#include "mapcontrol.h"
#include "maplayer.h"
#include "geometrylayer.h"
#include "arrowpoint.h"
#include "osmmapadapter.h"
#include "polygon.h"
#include "circle.h"

using namespace qmapcontrol;

namespace Ui {
	class MainWindow;
}
class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow( QWidget *parent = 0 );
	virtual ~MainWindow();

private:
	Ui::MainWindow *_ui;
	MapAdapter *_mapAdapter;
	MapControl *_mapControl;
	MapLayer *_mapLayer;
	GeometryLayer *_geometryLayer;

private slots:
	void mouseEventCoordinate( const QMouseEvent* event, const QPointF coordinate );
	void geometryClicked( Geometry* geometry, QPoint coord_px );

protected:
	void resizeEvent( QResizeEvent *event );

private:
	void test();
};

#endif // MAINWINDOW_H
