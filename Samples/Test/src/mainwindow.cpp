#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow( QWidget *parent )
	: QMainWindow( parent ),
	  _ui( new Ui::MainWindow )
{
	_ui->setupUi( this );
	_mapAdapter = new OSMMapAdapter();
	_mapLayer = new MapLayer( "mapLayer", _mapAdapter );
	_geometryLayer = new GeometryLayer( "geometryLayer", _mapAdapter );
	connect( _geometryLayer, SIGNAL(geometryClicked(Geometry*,QPoint)),
				 this, SLOT(geometryClicked(Geometry*,QPoint)) );
	_mapControl = new MapControl( QSize(), MapControl::Panning );
	_mapControl->addLayer( _mapLayer );
	_mapControl->addLayer( _geometryLayer );
	_mapControl->enableMouseWheelEvents( true );
	_mapControl->enablePersistentCache();
	_mapControl->showScale( true );
	this->setCentralWidget( _mapControl );
	connect( _mapControl, SIGNAL(mouseEventCoordinate(const QMouseEvent*,QPointF)),
			    this, SLOT(mouseEventCoordinate(const QMouseEvent*,QPointF)) );

	test();
}

MainWindow::~MainWindow()
{
	delete _mapControl;
	delete _ui;
}

void MainWindow::mouseEventCoordinate( const QMouseEvent *event, const QPointF coordinate )
{
	qDebug() << coordinate.x() << coordinate.y();
}

void MainWindow::geometryClicked( Geometry* geometry, QPoint coord_px )
{
	if( geometry->GeometryType == "Polygon" )
	{
		qDebug() << "polygonClicked";
	}
	else if( geometry->GeometryType == "Circle" )
	{
		qDebug() << "circleClicked";
	}
}

void MainWindow::resizeEvent( QResizeEvent *event )
{
	_mapControl->resize( event->size() );
}

void MainWindow::test()
{
	QPen *pen = new QPen();
		pen->setColor( QColor( 0, 255, 0, 50 ) );
		pen->setWidth( 3 );

	int p1y = -81.0;
	int p2y = -74.0;
	int p3y = -79.0;

	for( int i=0; i < 10; i++ )
	{
		QList<Point *> polygonPoints;
			polygonPoints.append( new Point( 53.0, p1y ) );
			polygonPoints.append( new Point( 60.0, p2y ) );
			polygonPoints.append( new Point( 80.0, p3y ) );
		Polygon *polygon = new Polygon( polygonPoints, QString(), true, pen );
		_geometryLayer->addGeometry( polygon );
		p1y += 10;
		p2y += 10;
		p3y += 10;
	}

	Circle *circle = new Circle( 37.7178, 55.6137, 500, QString(), true, pen );
	_geometryLayer->addGeometry( circle );

}
